import email
from lib import quopri
from email.utils import parsedate_tz
from re import sub
from decimal import Decimal
import string
import re

def get_html(message_body):

    msg = email.message_from_string(message_body)
    if msg.is_multipart():

        for part in msg.get_payload():

            if part.get_content_type() == 'text/html':

                charset = part.get_content_charset()
                html = unicode(part.get_payload(decode=True), str(charset), "ignore").encode('utf8', 'replace')
                strip_html = html.translate(string.maketrans("\n\t\r", "   "))

                return strip_html
    else:
        text = unicode(msg.get_payload(decode=True), 'utf-8', 'ignore').encode('utf8', 'replace')
        return text.strip()

def get_text(message_body):

    msg = email.message_from_string(message_body)
    for part in msg.get_payload():

        if part.get_content_type() == 'text/plain':
            return part.get_payload()

def get_date(message_body):

    email_message = email.message_from_string(message_body)
    email_date = parsedate_tz(email_message['date'])

    return email_date


def get_from(message_body):

    email_message = email.message_from_string(message_body)
    return email_message['from']

def get_subject(message_body):

    email_message = email.message_from_string(message_body)
    return email_message['subject']


def decode_message(message_body):

    return quopri.decodestring(message_body)

def get_int(s):
    try:
        return int(s)
    except ValueError:
        return 0

def get_digital(c):

    str = sub(r'[^\d.]', '', c)
    pos1 = str.find(".")
    if pos1 > 0:
        return float(str[:pos1+3])
    else:
        return float(str)

def write_html(f, message_body):
    path = "C:\\Users\\HUANG\\Documents\\Utrix\\Parser\\dist\\" + f
    f = open(path, 'a+')
    f.write(message_body) # python will convert \n to os.linesep
    f.close()

def exact_Match(phrase, word):
    r = re.findall('\\b'+word+'\\b', phrase, re.I)
    if len(r)>0:
        return True
    else:
        return False