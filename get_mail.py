import imaplib
import parse
import time
import helper
import datetime
from pymongo import MongoClient
from sys import argv
from bson.objectid import ObjectId

script, user_email, user_token, last_sync_date, user_id, provider = argv

start_time = time.time()

auth_string = 'user=%s\1auth=Bearer %s\1\1' % (user_email, user_token)

mail = imaplib.IMAP4_SSL('imap.gmail.com')
mail.debug = 4
mail.authenticate('XOAUTH2', lambda x: auth_string)
mail.select("[Gmail]/All Mail", readonly=True)

search_tag = ["purchase", "order", "ordered", "shipment", "shipped", "invoice", "confirmed", "confirmation",
              "notification", "receipt", "return", "pre-order", "pre-ordered", "payment", "on its way", "received"
              "fulfilled", "ereceipt", "package", "placed", "shipping"]

search_text = ''
for s in search_tag:
    search_text += '(SUBJECT "' + s.title() + '") '
    search_text += '(SUBJECT "' + s + '") '


search_criteria = '(' +\
                  'OR ' * ((len(search_tag)*2)-1) + search_text.strip() + ' (NOT SUBJECT "<ADV>"))'

#search_criteria = '(SUBJECT "[Qoo10] Your payment has been completed.")'
'''
typ, data = mail.search(None,  search_criteria)
'''

if last_sync_date == "":
    typ, data = mail.search(None,  search_criteria)
else:

    my_date = datetime.datetime.strptime(last_sync_date, "%Y-%m-%d %H:%M:%S.%f")
    since_date = '(SINCE "%s")' % my_date.strftime("%d-%b-%Y")
    typ, data = mail.search(None, since_date, search_criteria)


for num in data[0].split():
    try:
        typ, data = mail.fetch(num, '(RFC822)')
        message_body = helper.decode_message(str(data[0][1]))
        parse.parse_message(message_body, user_id)

    except Exception:
        print("err")


#close off and logout the connection to prevent simultaneous connection.
#Gmail only allow 10 simultaneous connection
mail.close()
mail.logout()

#update last sync mailbox datetime
client = MongoClient()
db = client['keepinorder']
db.AspNetUsers.update({"_id" : ObjectId(user_id), "Claims.ClaimType": "GoogleLastSyncOn"},
                      {"$set": {"Claims.$.ClaimValue": str(datetime.datetime.now())}}
                      )

end_time = time.time()
print("time: %g" % (end_time - start_time))


'''Remove to close the console window'''
raw_input("")
