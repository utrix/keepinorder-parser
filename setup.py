from distutils.core import setup
import py2exe

#setup(console=['get_mail.py'])

setup(
    console =[{'script': 'get_mail.py'}],
    options ={
        'py2exe':
        {
            'includes': ['lxml.etree', 'lxml._elementpath', 'gzip'],
        }
    }
)