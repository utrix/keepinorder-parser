import helper
import config
from lib import merchantA
from lib import merchantB
from lib import merchantC

'''
Merchant A are those mail that we seperate it by items
Merchant B are those mail that we aggreate all items into one

'''

def parse_message(message_body, user_id):

    subject = helper.get_subject(message_body)
    from_merchant = helper.get_from(message_body)
    mail_date = helper.get_date(message_body)

    print(subject)

    if 'Your payment has been completed' in subject:
    #if "qoo10cs@qoo10.sg" in from_merchant:
        helper.write_html("qoo10.html", message_body)
        merchantA.parse_html(config.qoo10_dict, message_body, mail_date, user_id, from_merchant)

    if 'Receipt for Your Payment' in subject:
    #elif "service@paypal.com.sg" in from_merchant:
        merchantA.parse_html(config.paypal_dict, message_body, mail_date, user_id, from_merchant)
        #paypal.parse_html(message_body, mail_date)

    elif "Order Confirmation" in subject:
        merchantB.parse_html(config.macdonald_dict, message_body, mail_date, user_id, from_merchant)

    elif "Your Pizza Hut online order is on its way!" in subject:
        merchantB.parse_html(config.pizzahut_dict, message_body, mail_date, user_id, from_merchant)

    elif "do_not_reply@itunes.com" in from_merchant:
        merchantA.parse_html(config.itunes_dict, message_body, mail_date, user_id, from_merchant)

    elif "customerservice@sistic.com.sg" in from_merchant:
        #helper.write_html("sistec.html", message_body)
        merchantA.parse_html(config.sistec_dict, message_body, mail_date, user_id, from_merchant)
    else:
        merchantC.parse_html(message_body, mail_date, user_id, from_merchant)








