import nltk
from nltk.tag.simplify import simplify_wsj_tag
import pickle
from nltk.classify import MaxentClassifier

def get_features(text):
    words = []
    # Same steps to start as before
    sentences = nltk.sent_tokenize(text)
    for sentence in sentences:
        words = words + nltk.word_tokenize(sentence)

    # part of speech tag each of the words
    pos = nltk.pos_tag(words)
    # Sometimes it's helpful to simplify the tags NLTK returns by default.
    # I saw an increase in accuracy if I did this, but you may not
    # depending on the application.
    pos = [simplify_wsj_tag(tag) for word, tag in pos]
    # Then, convert the words to lowercase like before
    words = [i.lower() for i in words]
    # Grab the trigrams
    trigrams = nltk.trigrams(words)
    # We need to concatinate the trigrams into a single string to process
    trigrams = ["%s %s %s" % (i[0], i[1], i[2]) for i in trigrams]
    # Get our final dict rolling
    features = words + pos + trigrams
    # get our feature dict rolling
    features = dict([(i, True) for i in features])
    return features




# Set up our training material in a nice dictionary.
training = {
    'ingredients': [
        'pacific rim',
        'Shaw Online',
        '3 eggs',
        '1/4 cup sugar',
    ],
    'steps': [
        'Sift the powdered sugar and cocoa powder together.',
        'Coarsely crush the peppercorns using a mortar and pestle.',
        'While the vegetables are cooking, scrub the pig ears clean and cut away any knobby bits of cartilage so they will lie flat.',
        'Heat the oven to 375 degrees.',
    ]
}

# Set up a list that will contain all of our tagged examples,
# which we will pass into the classifier at the end.
training_set = []
for key, val in training.items():
    for i in val:
        # Set up a list we can use for all of our features,
        # which are just individual words in this case.
        feats = []
        # Before we can tokenize words, we need to break the
        # text out into sentences.
        sentences = nltk.sent_tokenize(i)
        for sentence in sentences:
            feats = feats + nltk.word_tokenize(sentence)

        # For this example, it's a good idea to normalize for case.
        # You may or may not need to do this.
        feats = [i.lower() for i in feats]
        # Each feature needs a value. A typical use for a case like this
        # is to use True or 1, though you can use almost any value for
        # a more complicated application or analysis.
        feats = dict([(i, True) for i in feats])
        # NLTK expects you to feed a classifier a list of tuples
        # where each tuple is (features, tag).
        training_set.append((feats, key))

# Train up our classifier
classifier = MaxentClassifier.train(training_set)

# Test it out!
# You need to feed the classifier your data in the same format you used
# to train it, in this case individual lowercase words.


# Try it out
text = "Thank you for your ticket(s) purchase with Shaw Online. Your online transaction was successful.\r\nYour transaction details are as follows:\r\n\r\n\r\nShow Information\r\n----------------\r\nShow Date and Time : Sunday, 21 Jul 2013 09:30 PM\r\nCineplex / Cinema : Shaw Theatres JCube / JCUBE THEATRE IMAX\r\nCineplex Address : Shaw Theatres JCube,  2 Jurong East Central 1, JCube,  #04-11, Singapore 609731\r\nhttp://www.shaw.sg/sw_cinemadetails.aspx?cplexCode\u00118+252+24+185+6+252+92+164+68+152+162+30+15+253+217+106\r\nFilm Title : Pacific Rim   [IMAX 3D]\r\nFilm Rating : PG13 - Parental Guidance 13\r\nFilm Consumer Advice : Violence\r\nDuration : 131 minutes\r\nSeat Number : B:14|B:15|\r\n\r\nTransaction Information\r\n-----------------------\r\nTransaction Number : 10646279\r\nTransaction Date and Time : Saturday, 20 Jul 2013 10:58 PM\r\nNumber of Ticket(s) : 2\r\nPrice Per Ticket (S$) : 22.00\r\nBooking Fee (S$) : 1.00\r\nGrand Total (S$) : 45.00 (inclusive of GST)\r\n\r\n\r\nKindly note that no refunds, cancellations or exchanges will be entertained even if the tickets are not collected at the box office.\r\nPlease produce your credit card or quote transaction number to collect your ticket(s) at the box office. Enjoy your movies!\r\n\r\n\r\n---------------------------------------------------------------------------------------\r\nThis is a computer-generated responses and does not require a reply.\r\nVisit Shaw Online now! - http://www.shaw.sg\r\n---------------------------------------------------------------------------------------"
text = text.replace("\r\n"," ")
results = get_features(text)


for k, v in results.items():
    d={}
    d[k]=v;
    r = (classifier.classify(d))
    if "ingredients"==r:
        print k

#print classifier.classify(results)