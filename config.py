paypal_dict = { 'orders_item_table' : '//td[text()="Description"]/../../tr[position()>1]',
           'total_price1': '//td[text()="From amount"]/following-sibling::td[1]/text()',
           'total_price2': '//span[text()="Payment"]/../following-sibling::td[1]/text()',
           'merchant': '//span[text()="Merchant"]/../text()[preceding-sibling::br and following-sibling::br]',
           'order_description' : 'td[1]//text()',
           'order_quantity' : 'td[3]//text()',
           'order_price': 'td[4]//text()',
           'order_brand' : '/none',
           'payment': 'PayPal',
           'currency':'SGD'
           }

qoo10_dict = { 'orders_item_table' : '//td[contains(text(),"Order no")]/../../tr[position()>1 and position() < last()]',
           'total_price1': '//font[@color="red"]//text()',
           'total_price2': '//font[@color="red"]//text()',
           'merchant': 'Qoo10',
           'order_description' : 'td[2]//text()',
           'order_quantity' : 'td[3]//text()',
           'order_price': 'td[4]//text()',
           'order_brand' : 'td[5]//text()',
           'payment': 'Qoo10',
           'currency':'SGD'
           }

macdonald_dict = { 'orders_item_table' : '//td//b[contains(text(),"Item")]/../../../../tbody/tr',
           'total_price1': '//b[contains(text(),"Order Total")]/following-sibling::text()[1]',
           'total_price2': '//b[contains(text(),"Order Total")]/following-sibling::text()[1]',
           'merchant': 'McDelivery',
           'order_description' : 'td[1]//text()',
           'order_quantity' : 'td[2]//text()',
           'order_price': 'td[3]//text()',
           'order_brand' : '/none',
           'payment': 'McDelivery',
           'currency':'SGD'
           }

pizzahut_dict = { 'orders_item_table' : '//th[contains(text(),"Description")]/../../../tbody/tr[position()>1 and position() < last()-4]',
           'total_price1': '//td[contains(text(),"Total includes of GST")]/following-sibling::td[1]/text()',
           'total_price2': '//b[contains(text(),"Total includes of GST")]/following-sibling::td[1]/text()',
           'merchant': 'Pizza Hut',
           'order_description' : 'td[1]//text()',
           'order_quantity' : 'td[2]//text()',
           'order_price': 'td[3]//text()',
           'order_brand' : '/none',
           'payment': 'Pizza Hut',
           'currency':'SGD'
           }

itunes_dict = { 'orders_item_table': '//td/font/b[contains(text(),"Item")]/../../../../..//tr[position()>4 and position()<last()-6]',
           'total_price1': '//font[contains(text(),"Order Total")]/../following-sibling::td[1]/font/text()',
           'total_price2': '//font[contains(text(),"Order Total")]/../following-sibling::td[1]/font/text()',
           'merchant': '//b[contains(text(),"Apple Inc.")]//text()',
           'order_description': 'td[1]//font//text()[1]',
           'order_quantity': '/none',
           'order_price': 'td[4]//font//text()',
           'order_brand': 'td[2]//font//text()',
           'payment': 'Apple Inc',
           'currency':'SGD'
           }

sistec_dict = { 'orders_item_table' : '//table[2]//tr[position()>3 and position()<last()-3]',
           'total_price1': '//div[contains(text(),"TOTAL")]/../following-sibling::td[1]/div/text()',
           'total_price2': '//div[contains(text(),"TOTAL")]/../following-sibling::td[1]/div/text()',
           'merchant': 'SISTIC Singapore',
           'order_description' : 'td[2]/span/b/text()',
           'order_quantity' : 'td[4]//div//text()',
           'order_price': 'td[7]//div//text()',
           'order_brand' : '/none',
           'payment': 'SISTIC',
           'currency':'SGD'
           }