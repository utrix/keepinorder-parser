import helper
import re
from pymongo import MongoClient
import email.utils
import logging



def parse_html(message_body, mail_date, user_id, from_merchant):

    #FORMAT = "%(asctime)-15s %(clientip)s %(user)-8s %(message)s"
    #logging.basicConfig(filename='C:\Users\HUANG\Documents\Utrix\Parser\dist\error.log',  level=logging.DEBUG, format='%(asctime)s %(message)s')

    try:

        #insert into MongoDB
        client = MongoClient()
        db = client['keepinorder']

        html_message = helper.get_html(message_body)
        merchant = []

        from_merchant = email.utils.parseaddr(from_merchant)
        if from_merchant[0] == "":
            merchant.append(str(from_merchant[1]).split("@")[1])
            merchant.append(str(from_merchant[1]))
        else:
            merchant.append(str(from_merchant[0]))
            merchant.append(str(from_merchant[1]))

        rid = db.RawReceipts.insert({"html": html_message, "merchant": merchant, "user_id":user_id})


        order_item = []
        if (helper.exact_Match(html_message, "transaction")
            or helper.exact_Match(html_message, "order")
            or helper.exact_Match(html_message, "receipt")
            or helper.exact_Match(html_message, "confirmation")) \
            and helper.exact_Match(html_message, "total"):

            find_currency = re.findall(ur'[$](\s?\d+(?:\.\d{2})?)', html_message.replace(",", ""))
            result = map(float, find_currency)

            order_item.append({"description": "", "quantity": 0, "price": max(result), "brand": "", "category": "", "ItemId":"1"})

            json = {"UserId": user_id, "Merchant": merchant,
                    "OrderItem": order_item,
                    "TotalPrice": max(result),
                    "Receipt": rid,
                    "Payment": merchant[0],
                    "Currency": "SGD",
                    "OrderDate": mail_date}

            mid = db.Orders.insert(json)
            client.close()

            print(mid)
            return mid


    except:
        print 'error'
        #logging.exception("error")