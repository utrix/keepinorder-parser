import helper
import lxml.html
from pymongo import MongoClient
import logging
import string


def parse_html(paypal_dict, message_body, mail_date, user_id, from_merchant):

    #FORMAT = "%(asctime)-15s %(clientip)s %(user)-8s %(message)s"
    #logging.basicConfig(filename='C:\Users\HUANG\Documents\Utrix\Parser\dist\error.log',  level=logging.DEBUG, format='%(asctime)s %(message)s')

    try:

        #insert into MongoDB
        client = MongoClient()
        db = client['keepinorder']

        html_message = helper.get_html(message_body)
        rid = db.RawReceipts.insert({"html": html_message, "merchant": from_merchant, "user_id":user_id})
        tree = lxml.html.fromstring(html_message)

        #Order Items
        orders_item_table = tree.xpath(paypal_dict['orders_item_table'])

        #Total Price
        total_price = tree.xpath(paypal_dict['total_price1'])
        if not total_price:
            total_price = tree.xpath(paypal_dict['total_price2'])

        #Merchant
        merchant = [paypal_dict['merchant']]

        order_item = []

        if len(orders_item_table) > 0:

            description_str = ""

            for item in orders_item_table:

                description = item.xpath(paypal_dict['order_description'])
                for d in description:
                    description_str += d.strip() + ", "

            order_item.append({"description": description_str, "quantity": 0, "price": helper.get_digital(total_price[0]), "brand": "", "category": "", "ItemId":"1"})

            json = {"UserId": user_id, "Merchant": merchant,
                    "OrderItem": order_item,
                    "TotalPrice": helper.get_digital(total_price[0]),
                    "Receipt": rid,
                    "Payment": paypal_dict['payment'],
                    "Currency": "SGD",
                    "OrderDate": mail_date}

            mid = db.Orders.insert(json)
            client.close()

            return mid

    except:
        print('error')
        #logging.exception("error")