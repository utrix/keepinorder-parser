import helper
import lxml.html
from pymongo import MongoClient
import logging
import string

def parse_html(merchant_dict, message_body, mail_date, user_id, from_merchant):

    #logging.basicConfig(filename='C:\Users\HUANG\Documents\Utrix\Parser\dist\error.log',  level=logging.DEBUG, format='%(asctime)s %(message)s')

    try:

        #insert into MongoDB
        client = MongoClient()
        db = client['keepinorder']

        html_message = helper.get_html(message_body)
        rid = db.RawReceipts.insert({"html": html_message, "merchant": from_merchant, "user_id":user_id})
        tree = lxml.html.fromstring(html_message)

        #Order Items
        orders_item_table = tree.xpath(merchant_dict['orders_item_table'])



        #Total Price
        total_price = tree.xpath(merchant_dict['total_price1'])
        if not total_price:
            total_price = tree.xpath(merchant_dict['total_price2'])

        #Merchant
        if '//' in merchant_dict['merchant']:
            merchant = tree.xpath(merchant_dict['merchant']) #is a xpath
        else:
            merchant = [merchant_dict['merchant']]

        order_item = []

        print(len(orders_item_table))

        if len(orders_item_table) > 0:

            orderitem_id = 0

            for item in orders_item_table:
                description_str = ""
                brand_str = ""
                quantity_int = 0
                price_int=0

                description = item.xpath(merchant_dict['order_description'])
                for d in description:
                    description_str += d.strip()

                quantity = item.xpath(merchant_dict['order_quantity'])
                for q in quantity:
                    quantity_int += helper.get_int(q)

                price = item.xpath(merchant_dict['order_price'])

                brand = item.xpath(merchant_dict['order_brand'])
                for b in brand:
                    brand_str += b.strip()

                orderitem_id += 1
                order_item.append({"ItemId":  str(orderitem_id), "description": description_str, "quantity": quantity_int,
                                   "price": helper.get_digital(price[0]), "brand": brand_str, "category": ""})

            json = {"UserId": user_id,
                    "Merchant": merchant,
                    "OrderItem": order_item,
                    "TotalPrice": helper.get_digital(total_price[0]),
                    "Receipt": rid,
                    "Payment": merchant_dict['payment'],
                    "Currency": "SGD",
                    "OrderDate": mail_date}

            mid = db.Orders.insert(json)
            client.close()

            return mid


    except:
        print("error")
        #logging.exception("error")